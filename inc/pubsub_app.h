/***********************************************************************
*
*            Copyright 2010 Mentor Graphics Corporation
*                         All Rights Reserved.
*
* THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION WHICH IS
* THE PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS AND IS
* SUBJECT TO LICENSE TERMS.
*
************************************************************************

************************************************************************
*
* FILE NAME
*
*       pubsub_app.h
*
* DESCRIPTION
*
***********************************************************************/
#ifndef _CSPBOX_CONF_APP_H_
#define _CSPBOX_CONF_APP_H_

#include <memory>
#include <thread>
#include <cspeappsdk/cspeappagent/appagent.h>
#include <cspeappsdk/cspeappagent/appconfig.h>

class PubSubApplication {
    enum {
        STATE_STOPPED = 0,
        STATE_STARTED, 
        STATE_REMOVED,
        STATE_ERROR
    }; 
public:
    PubSubApplication();
    ~PubSubApplication();
    bool initialize();
    void log(const std::string &msg);

    // CSP Application Agent Callback Handlers
    CSP_VOID initializeResponse(const INIT_RESPONSE &res);
    CSP_VOID getConfigResponse(cspeapps::sdk::AppConfig config);
    CSP_VOID beSignallingRequest(cspeapps::sdk::AppSignal signal);
private:
    CSP_STRING getContainerId();
    CSP_VOID sendApplicationState(const CSP_LONG &state);
    CSP_VOID sendNetworkInfo();

    CSP_STRING getIpAddress();
    CSP_BOOL restartListner(const CSP_STRING &port);
public:
    std::unique_ptr<cspeapps::sdk::AppAgent> AGENT;
    std::unique_ptr<cspeapps::sdk::AppConfig> CONFIG;
    std::unique_ptr<std::thread> _bannerPrinter;

    CSP_STRING _lastJobId;
    CSP_STRING _host_ip;
    CSP_STRING _bind_port;
    CSP_LONG   _app_state;

    volatile CSP_BOOL isRunning;
private:
    static const CSP_STRING BIND_PORT_PARAM_TAG; 
};

#endif /* _CSPBOX_CONF_APP_H_ */
