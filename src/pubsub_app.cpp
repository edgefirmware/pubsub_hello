/***********************************************************************
 *
 *            Copyright 2010 Mentor Graphics Corporation
 *                         All Rights Reserved.
 *
 * THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION WHICH IS
 * THE PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS AND IS
 * SUBJECT TO LICENSE TERMS.
 *
 ************************************************************************

 ************************************************************************
 *
 * FILE NAME
 *
 *       pubsub_app.cpp
 *
 * DESCRIPTION
 *
 *
 ***********************************************************************/
#include <ctime>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>

#include <ctype.h>
#include <signal.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>

#include <cspeappsdk/cspeappagent/apperror.h>
#include "pubsub_app.h"

// hello_interval tag is the managed application parameter tag given while creating
// the application metadata by the Application Developer using the CSP Platform Application Portal
const CSP_STRING PubSubApplication::BIND_PORT_PARAM_TAG = "pubsub_bind_port"; 

PubSubApplication::PubSubApplication() : AGENT(nullptr), 
    _bannerPrinter(nullptr), _lastJobId(""), _host_ip("127.0.0.1"), _bind_port("4445"), _app_state(0), isRunning(true) 
{

}

PubSubApplication::~PubSubApplication()
{
    isRunning = false;
}

bool PubSubApplication::initialize()
{
    // AppAgent object is the primary interface to all the workflow operations 
    // performed by the CSP Agent
    this->AGENT = std::unique_ptr<cspeapps::sdk::AppAgent>(new cspeapps::sdk::AppAgent());

    // Before even initializing the Agent, we will register the Signalling Callback because
    // the application will start to receive BE signals as soon as the Agent is initialized
    // to make sure we do not miss any BE signal, we will register our handler now.
    this->AGENT->RegisterBESignalCallback(std::bind(&PubSubApplication::beSignallingRequest, this, std::placeholders::_1));

    // Initialize the agent. 
    this->AGENT->Initialize(std::bind(&PubSubApplication::initializeResponse, this, std::placeholders::_1));
}

void PubSubApplication::log(const std::string &msg)
{
    std::cout << "[PUBSUB-HELLO] : " << msg << std::endl;
}

CSP_VOID PubSubApplication::initializeResponse(const INIT_RESPONSE &res)
{
    if ( res.status ) {
        log("Agent initialized successfully");
        log("Getting Application Configuration from CSP Platform BE");

        // Once we are initialized successfully, our first task is to get the configuration of the application
        // from the BE so that we can start our application. 
        this->AGENT->GetConfiguration(std::bind(&PubSubApplication::getConfigResponse, this, std::placeholders::_1));
    } else {
        log("Initialization Failed");
    }
}
CSP_VOID PubSubApplication::getConfigResponse(cspeapps::sdk::AppConfig config)
{
    log("Received configuration from BE");
    CONFIG.reset();
    CONFIG = std::unique_ptr<cspeapps::sdk::AppConfig>(new cspeapps::sdk::AppConfig(config));

    // Get configuration
    CSP_STRING _bind_port_rq = CONFIG->GetRequestedValue(BIND_PORT_PARAM_TAG);

    CSP_STRING _bind_port_cr = CONFIG->GetCurrentValue(BIND_PORT_PARAM_TAG);

    log("Bind Port Requested Value = " + _bind_port_rq);
    log("Bind Port Current Value = " + _bind_port_cr);

    // Set Bind Port
    CSP_STRING new_port = "";
    if ( _bind_port_rq.length() > 0 ) {
        new_port = _bind_port_rq;
    } else {
        new_port = _bind_port_cr;
    }
    if ( restartListner(new_port) ) {
        log("Successfully started listner with the input port");
    } else {
        log("Failed to start listner with the input port");
    }
    CONFIG->SetCurrentValue(BIND_PORT_PARAM_TAG, _bind_port, "new value applied");

    // Report configuration to BE
    AGENT->ReportConfiguration(*CONFIG, nullptr);

    // Update application state
    // sendApplicationState(STATE_STARTED);

    // sendNetworkInfo();
}
CSP_VOID PubSubApplication::beSignallingRequest(cspeapps::sdk::AppSignal signal)
{
    // CSP Platform BE Signal handler
    log("Received a signal from BE");
    _lastJobId = signal.GetJobId();

    // Currently we only have one operation as implemented below.
    if ( signal.GetRequestedOperation() == "update_configuration" ) {
        // This operation does not have any parameters, so we are just taking
        // appropriate action to service this request
        // Since the signal is asking us to update the configuration, so we will 
        // just call the GetConfiguration API again.
        this->AGENT->GetConfiguration(std::bind(&PubSubApplication::getConfigResponse, this, std::placeholders::_1));
    }
    log("Signal handling completed");
}

CSP_STRING PubSubApplication::getIpAddress()
{
     int fd;
     struct ifreq ifr;

     fd = socket(AF_INET, SOCK_DGRAM, 0);

     /* I want to get an IPv4 IP address */
     ifr.ifr_addr.sa_family = AF_INET;

     /* I want IP address attached to my interface */
     strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);

     ioctl(fd, SIOCGIFADDR, &ifr);

     close(fd);

     CSP_STRING ipaddr(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

     return ipaddr;
}

CSP_BOOL PubSubApplication::restartListner(const CSP_STRING &port)
{
    CSP_BOOL status = true;

    _bind_port = port;

    // Also set the new state of the application
    _app_state = STATE_STARTED;

    return status;
}

CSP_VOID PubSubApplication::sendApplicationState(const CSP_LONG &state)
{
    log("Sending application status to BE");
    CSP_STRING status_s = "";
    switch(state) {
    case STATE_STOPPED:
        status_s = "Stopped";
    break;
    case STATE_STARTED:
        status_s = "Started";
    break;
    case STATE_REMOVED:
        status_s = "Removed";
    break;
    case STATE_ERROR:
        status_s = "Error";
    break;
    default:
        status_s = "Unknown";
    break;
    }
    // Create Application Packet
    APP_PACKET packet;
    packet.jobid = "1";
    packet.method = "POST";
    packet.api = "/gateway-services/containerapps/v1/" + getContainerId() + "/status";
    packet.body = "{\"status\":\"" + status_s + "\"}";

    // Make the call
    if ( this->AGENT->BEApiCall(packet, nullptr) ) {
        log("BE API called submitted to queue successfully");
    } else {
        log("BE API called failed to be submitted to queue");
    }
}

CSP_VOID PubSubApplication::sendNetworkInfo()
{
    log("Sending networking information to BE");
    CSP_STRING address = getIpAddress();

    // Create Application Packet
    APP_PACKET packet;
    packet.jobid = "1";
    packet.method = "POST";
    packet.api = "/gateway-services/containerapps/v1/" + getContainerId() + "/networksettings";
    packet.body = "{\"privateIp\":\"" + address + "\", \"privatePort\":\"\"}";

    // Make the call
    if ( this->AGENT->BEApiCall(packet, nullptr) ) {
        log("BE API called submitted to queue successfully");
    } else {
        log("BE API called failed to be submitted to queue");
    }

}

CSP_STRING PubSubApplication::getContainerId()
{
    CSP_STRING id = "";
    CSP_CHAR* ID = getenv("HOSTNAME");
    if ( ID != NULL ) {
        id = CSP_STRING(ID);
    }

    return id;
}
